import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('pkg', [
  'openssh-server'
])
def test_pkg(host, pkg):
    package = host.package(pkg)
    assert package.is_installed


def test_ssh_is_running(host):
    ssh = host.service('ssh')

    assert ssh.is_running


@pytest.mark.parametrize('file, content', [
  ("/etc/ssh/sshd_config", "PasswordAuthentication no"),
  ("/etc/ssh/sshd_config", "PermitRootLogin no")
])
def test_files(host, file, content):
    file = host.file(file)

    assert file.exists
    assert file.contains(content)
