ansible-role-ssh
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-ssh/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-ssh/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

This role configures SSH to be more secure.

Requirements
------------
No special requirements.

Role Variables
--------------
see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------
none

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: ansible-role-ssh, x: 42 }

*Inside `vars/main.yml`*:

             sudoers_passworded:
               - jane
               - deployer


Suported Platforms
--------------
Debian 9 (Stretch)


License
-------

BSD
